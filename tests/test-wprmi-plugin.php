<?php
/**
 * Class WPRMITest
 *
 * @package Wprocket_Multiple_Instances
 */

/**
 * Sample test case.
 */
class WPRMITest extends WP_UnitTestCase{

    public function setUp()
    {
        parent::setUp();
    }
    /**
     * Test instance id
     */
    public function test_instance_id(){
        $this->assertEquals('TEST_ENV', wprmi_get_instance_id());
    }
    /**
     * Test add to transient function
     */
    public function test_add_to_transient(){
        
        $this->assertFalse( get_transient('test_transient') );

        wprmi_add_to_transient('test_transient');

        $this->assertContains('TEST_ENV', get_transient('test_transient') );
        // Test exists in function
        $this->assertTrue( wprmi_exists_in('test_transient') );
    }
    /**
     * Test if returns false when it doesn't exists
     */
    public function test_exists_in_reverse(){
        $this->assertFalse( wprmi_exists_in('test_transient') );
    }
    /**
     * Test purge all
     */
    public function test_purge_to_all_instances(){

        $this->assertFalse( get_transient('wprmi_purge_all') );
        $this->assertFalse( get_transient('wprmi_purged_instances') );
        
        wprmi_purge_to_all_instances();
        
        $this->assertEquals( '1', get_transient('wprmi_purge_all') );
        $this->assertContains( 'TEST_ENV', get_transient('wprmi_purged_instances') );
    }
    /**
     * Test regenerate all
     */
    public function test_regenerate_to_all_instances(){
        
        $this->assertFalse( get_transient('wprmi_regenerate_all') );
        $this->assertFalse( get_transient('wprmi_regenerated_instances') );

        wprmi_regenerate_to_all_instances();
        
        $this->assertEquals( '1', get_transient('wprmi_regenerate_all') );
        $this->assertContains( 'TEST_ENV', get_transient('wprmi_regenerated_instances') );
    }
}
