<?php
/*
Plugin Name: WP Rocket multiple instances
Description: Sets transient so that load-balanced servers receive cache purge requests across all instances
Version: 1.0
Author: Nuno Duarte <nuno@hanzo.es>
Author URI: https://hanzo.es/
License: http://www.apache.org/licenses/LICENSE-2.0
Text Domain: wprocket-multiple-instances
Copyright 2019: Nuno Duarte <nuno@hanzo.es>
*/
/**
 * 
 * Create transient so all instances can check and override.
 * Runs when WP Rocket purge cache is called. 
 * It's a manual action and should only be done in one of the instances.
 * 
 */
function wprmi_purge_to_all_instances(){
    
    $existingTransient = get_transient('wprmi_purge_all');
    $expiresOn = 15 * 60;

    // If existing transient, then do nothing. 
    // This will make it 15 min until possibility of next cache purge!
    // maybe block purge button and show notice
    if($existingTransient) return;
    // Set new transient
    set_transient('wprmi_purge_all', 1, $expiresOn);
    // Add this instance to purged list
    wprmi_add_to_transient('wprmi_purged_instances');
    
    //wprmi_log();
}
add_action('after_rocket_clean_domain', 'wprmi_purge_to_all_instances', 100);
//add_action('after_rocket_clean_post', 'wprmi_purge_to_all_instances', 100);
/**
 *
 * Create transient so all instances can check and override.
 * Runs when WP Rocket options change
 * It's a manual action and should only be done in one of the instances.
 *
 */
function wprmi_regenerate_to_all_instances(){

    $existingTransient = get_transient('wprmi_regenerate_all');
    $expiresOn = 15 * 60;
    // Set/update transient
    set_transient('wprmi_regenerate_all', 1, $expiresOn);
    // Add this instance to regenerated list
    wprmi_add_to_transient('wprmi_regenerated_instances');
    //wprmi_log();
}
add_action('update_option_' . WP_ROCKET_SLUG, 'wprmi_regenerate_to_all_instances', 10);
/**
 * 
 *  Check if instance should purge cache
 *  Hooked to the plugins_loaded which is the earliest useful hook on 
 *  WP since we need plugins loaded for this.
 * 
 */
function wprmi_should_do_purge(){
    if(get_transient('wprmi_purge_all') && !wprmi_exists_in('wprmi_purged_instances') ){
        // Clear cache.
        if (function_exists('rocket_clean_domain')) {
            rocket_clean_domain(); 
            // add to purged instances list
            wprmi_add_to_transient('wprmi_purged_instances'); 
        }
        // Trigger preload cache
        if (function_exists('run_rocket_sitemap_preload')) {
            run_rocket_sitemap_preload();
        }
    }
}
add_action('plugins_loaded','wprmi_should_do_purge', 5);
/**
 *
 * Regenerate files on all instances after settings change.
 *
 */
function wprmi_should_do_files_regenerate(){
    if(get_transient('wprmi_regenerate_all') && !wprmi_exists_in('wprmi_regenerated_instances')){
        // regenerate htaccess
        if(function_exists('flush_rocket_htaccess')){
            flush_rocket_htaccess();
        }
        // regenerate config file
        if(function_exists('rocket_generate_config_file')){
            rocket_generate_config_file();
        }
        // add to regenerated list
        wprmi_add_to_transient('wprmi_regenerated_instances');
    }
}
add_action('plugins_loaded', 'wprmi_should_do_files_regenerate', 5);
/**
 * 
 *  Check if instance is already listed
 * 
 */
function wprmi_exists_in($key){
    $transient = get_transient($key);
    $instanceID = wprmi_get_instance_id();

    if(!$transient || !is_array($transient)) return false;

    if(!$instanceID) return false;

    return in_array($instanceID, $transient) ? true : false;
}
/**
 * 
 * Setup/update transients purged list
 * 
 */
function wprmi_add_to_transient($key){
    $transient = get_transient($key);
    $transient = (!empty($transient) || is_array($transient) ) ? $transient : array();

    $instanceID = wprmi_get_instance_id();
    if (!$instanceID) return;

    $expiresOn = 15 * 60;

    if (!in_array($instanceID, $transient)) {
        $transient[] = $instanceID;
        set_transient($key,
            $transient,
            $expiresOn
        );
    }
}
/**
 * Retrieve instance identifier
 */
function wprmi_get_instance_id(){
    return defined('WPRMI_IDENTIFIER') ? WPRMI_IDENTIFIER : false;
}
/**
 * LOG
 */
function wprmi_log(){

    error_log('=========== START WPRMI LOG ==========');

    if(get_transient('wprmi_purge_all') ){
        error_log('PURGE ALL FLAG IS SET');
        error_log( 'LIST OF PURGED INSTANCES : ' . json_encode( get_transient('wprmi_purged_instances') ) );
    }
    if(get_transient('wprmi_regenerate_all') ){
        error_log('FILE REGENERATE FLAG IS SET');
        error_log('LIST OF REGENERTED INSTANCES : ' . json_encode( get_transient('wprmi_regenerated_instances') ) );
    }
    error_log('=========== END WPRMI LOG ==========');
    /*
    delete_transient('wprmi_purged_instances');
    delete_transient('wprmi_purge_all');
    delete_transient('wprmi_regenerated_instances');
    delete_transient('wprmi_regenerate_all');
    */
}
